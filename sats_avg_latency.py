import os
import re
import matplotlib.pyplot as plt
import sys
import numpy as np

START = 4
STEP = 2
SIZE = 30

def plot_avg_distance(distance_data):
    labels = list(range(START, SIZE+1, STEP))
    means = [np.average(step) for step in distance_data]
    errs = [np.std(step) for step in distance_data]
    maxs = [np.max(step) for step in distance_data]
    mins = [np.min(step) for step in distance_data]

    xs = np.arange(len(labels))
    width = 0.2

    fig, ax = plt.subplots()
    ax.scatter(xs, maxs, linewidths=0.5, marker='.', c='green')
    ax.bar(xs, means, width, yerr=errs, zorder=0, color='green')
    ax.scatter(xs, mins, linewidths=0.5, marker='.', c='black')

    ax.set_ylabel('Latencia [m]')
    ax.set_title(f'Latencia promedio entre los satélites')

    ax.set_xlabel('Nodos')
    ax.set_xticks(xs)
    ax.set_xticklabels(labels)
    ax.legend()

    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    log_files = sorted([filename for filename in os.listdir(".") if filename.startswith('planets_30_steps')], key=lambda v:int(v.split('_')[-1]))
    datas = []

    for filename in log_files:
        if filename == 'planets_30_steps_1': continue
        with open(filename, 'r') as f:
            input_file = f.read().strip().split('\n')
            header, *rows = input_file
        avg_latency = {}
        for row in rows:
            sat_from, sat_to, start_time, stop_time, min_latency, max_latency = row.split(',')
            if sat_from == sat_to: continue
            if not (sat_from, sat_to) in avg_latency:
                avg_latency[(sat_from, sat_to)] = []
            avg_latency[(sat_from, sat_to)].append((float(min_latency)+float(max_latency))/120.)
        print([(k, np.average(v)) for k, v in avg_latency.items()])
        sys.exit(0)
        datas.append([np.average(v) for k, v in avg_latency.items()])
    print(len(datas))
    plot_avg_distance(datas)

