planets = "Sat_Sun,Sat_Mercury,Sat_Venus,Sat_Moon,Sat_Mars,Sat_Deimos,Sat_Phobos,Sat_Ceres,Sat_Jupiter,Sat_Callisto,Sat_Europa,Sat_Ganymede,Sat_Io,Sat_Saturn,Sat_Mimas,Sat_Enceladus,Sat_Tethys,Sat_Dione,Sat_Rhea,Sat_Titan,Sat_Hyperion,Sat_Iapetus,Sat_Phoebe,Sat_Uranus,Sat_Ariel,Sat_Titania,Sat_Neptune,Sat_Triton,Sat_Pluto,Sat_Charon".split(',')

START = 4
STEP = 2

planet_distance_to_sun = {
    "Sun": 0,
    "Mercury": 0.39,
    "Venus": 0.72,
    "Earth": 1,
    "Mars": 1.52,
    "Ceres": 2.8,
    "Jupiter": 5.2,
    "Saturn": 9.54,
    "Uranus": 19.2,
    "Neptune": 30.06,
    "Pluto": 39.5
}


planet_to_sat = {
    "Sun": ["Sat_Sun"],
    "Mercury": ["Sat_Mercury"],
    "Venus": ["Sat_Venus"],
    "Earth": ["Sat_Moon"],
    "Mars": ["Sat_Mars","Sat_Deimos","Sat_Phobos"],
    "Ceres": ["Sat_Ceres"],
    "Jupiter": ["Sat_Jupiter","Sat_Callisto","Sat_Europa","Sat_Ganymede","Sat_Io"],
    "Saturn": ["Sat_Saturn","Sat_Mimas","Sat_Enceladus","Sat_Tethys","Sat_Dione","Sat_Rhea","Sat_Titan","Sat_Hyperion","Sat_Iapetus","Sat_Phoebe"],
    "Uranus": ["Sat_Uranus","Sat_Ariel","Sat_Titania"],
    "Neptune": ["Sat_Neptune","Sat_Triton"],
    "Pluto": ["Sat_Pluto","Sat_Charon"]
}

assert(set(planet_distance_to_sun.keys()) == set(planet_to_sat.keys()))

sat_to_planet = { sat : planet for sat in planet_to_sat[planet] for planet in planet_to_sat.keys() }

planet_steps = [ list(map(lambda v: planet_distance_to_sun[sat_to_planet[v]], planets[:i])) for i in range(START, len(planets)+1, STEP) ]
