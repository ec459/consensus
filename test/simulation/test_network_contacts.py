import datetime
import unittest

from consensus.simulation import network_contacts


class TestContactNetworkDelay(unittest.TestCase):
    def test_delay_after_last_contact(self):
        network_delay = network_contacts.ContactNetworkDelay(2, [
            network_contacts.Contact(source=0, destination=1, start=0, stop=60, min_latency=1, max_latency=1)])
        self.assertEqual(-1, network_delay.get_delay(0, 1, 100, ''))

    def test_delay_between_two_contacts(self):
        network_delay = network_contacts.ContactNetworkDelay(2, [
            network_contacts.Contact(source=0, destination=1, start=0, stop=60, min_latency=1, max_latency=1),
            network_contacts.Contact(source=0, destination=1, start=120, stop=150, min_latency=1, max_latency=1)])
        self.assertAlmostEqual(121, network_delay.get_delay(0, 1, 100, ''))

    def test_delay_in_contact(self):
        network_delay = network_contacts.ContactNetworkDelay(2, [
            network_contacts.Contact(source=0, destination=1, start=30, stop=60, min_latency=5, max_latency=5)])
        self.assertEqual(40, network_delay.get_delay(0, 1, 35, ''))

    def test_delay_same_node(self):
        network_delay = network_contacts.ContactNetworkDelay(2, [
            network_contacts.Contact(source=0, destination=1, start=30, stop=60, min_latency=5, max_latency=5)])
        self.assertEqual(0, network_delay.get_delay(0, 0, 35, ''))


class TestContact(unittest.TestCase):
    def test_from_csv(self):
        node_to_idx = {'DSN_Goldstone': 0, 'Sat_Ariel': 1}
        csv_row = ['DSN_Goldstone', 'Sat_Ariel', '31 Dec 2019 21:18:30.847', '1 Jan 2020 09:42:45.914', '9690', '9694']
        expected_contact = (network_contacts.Contact(source=0, destination=1,
                                                     start=datetime.datetime.strptime(csv_row[2] + '000',
                                                                                      network_contacts.DATETIME_FORMAT),
                                                     stop=datetime.datetime.strptime(csv_row[3] + '000',
                                                                                     network_contacts.DATETIME_FORMAT),
                                                     min_latency=9690 / 60., max_latency=9694 / 60.))

        contact = network_contacts.Contact.from_csv_row(node_to_idx, csv_row)

        self.assertEqual(expected_contact, contact)
