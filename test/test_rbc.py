import random

from pytest import raises

import simpy
from simpy.resources.store import Store

from consensus.core.reliablebroadcast import reliablebroadcast, encode, decode
from consensus.core.reliablebroadcast import hash, merkleTree, getMerkleBranch, merkleVerify


### Merkle tree
def test_merkletree0():
    mt = merkleTree(["hello"])
    assert mt == [b'', hash("hello")]


def test_merkletree1():
    strList = ["hello", "hi", "ok"]
    mt = merkleTree(strList)
    roothash = mt[1]
    assert len(mt) == 8
    for i in range(3):
        val = strList[i]
        branch = getMerkleBranch(i, mt)
        assert merkleVerify(3, val, roothash, branch, i)


### Zfec
def test_zfec1():
    K = 3
    N = 10
    m = b"hello this is a test string"
    stripes = encode(K, N, m)
    assert decode(K, N, stripes) == m
    _s = list(stripes)
    # Test by setting some to None
    _s[0] = _s[1] = _s[4] = _s[5] = _s[6] = None
    assert decode(K, N, _s) == m
    _s[3] = _s[7] = None
    assert decode(K, N, _s) == m
    _s[8] = None
    with raises(ValueError) as exc:
        decode(K, N, _s)
    assert exc.value.args[0] == 'Too few to recover'


### RBC
def simple_router(n, env, maxdelay=0.01, seed=None):
    rnd = random.Random(seed)
    queues = [Store(env) for _ in range(n)]

    def make_send(i):
        """Returns a function that sends a message within the SimPy environment."""

        def _send(j, o):
            if i != j:
                delay = rnd.random() * maxdelay
                yield env.timeout(delay)
            queues[j].put((i, o))

        return lambda j, o: env.process(_send(j, o))

    def make_recv(j):
        def _recv():
            i, o = yield queues[j].get()
            return i, o

        def _recv2(): return env.process(_recv())

        return _recv2

    return [make_send(i) for i in range(n)], [make_recv(j) for j in range(n)]


def test_rbc_on_network_without_bad_nodes(seed=24):
    N, f, sid, threads = 4, 1, 'sidA', []
    rnd = random.Random(seed)
    env = simpy.Environment()
    leader, leader_input = rnd.randint(0, N - 1), Store(env, capacity=1)
    sends, recvs = simple_router(N, env, seed=seed)

    for i in range(N):
        input = leader_input.get if i == leader else None
        threads.append(env.process(reliablebroadcast(sid, i, N, f, leader, input, recvs[i], sends[i])))
    m = "test_rbc1 message"
    leader_input.put(m)
    env.run()

    assert all([thread.processed for thread in threads])
    assert [t.value for t in threads] == [m] * N
