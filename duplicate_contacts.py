import datetime

with open('contacts/interplanetary-access-4weeks.csv') as f:
    contacts_file = [l.split(',') for l in f.read().strip().split('\n')]
ITERATION = 12
output_contacts = []
def increment_datetime(dt, months):
    time_format = r'%d %b %Y %H:%M:%S.%f'
    return (datetime.datetime.strptime(dt, time_format) + datetime.timedelta(days=35*months)).strftime(time_format)

for i in range(ITERATION):
    for c in contacts_file[1:]:
        start = increment_datetime(c[2], i)
        end = increment_datetime(c[3], i) 
        output_contacts.append([c[0], c[1], start, end, c[4], c[5]])

with open('contacts/interplanetary-access-4weeks-12x_copy.csv', 'w') as f:
    f.write(f'{",".join(contacts_file[0])}\n')
    f.write('\n'.join([','.join(c) for c in output_contacts]))
