#!/usr/bin/bash
# Warning: All previous simulation information will be deleted with `make clean`

export PYTHONPATH='.'

# BEATX and HBBFT redo for tmp2
# TODO: Increase for final results
ITERATION=1000

# Using N as 4*F
# B="100 10000 1000000"
B="120"
# B="100 500 1000 5000 10000 100000"
# Networks with throughput
#NW = "1 2"
# Network 0 (no throughput)
# Network 1 (256kbps all connections)
# Network 2 (32kbps all connections)
CONTACTS_FILE0="contacts/duplicated_x4_"
STEPS="0"
# Clean up all previous simulation data
#make clean


########################
## BEAT0-1 simulations
########################
BEAT_VERSIONS="0"

for s in $STEPS; do
# Network: CONTACTS_FILE0
for b in $B; do
for v in $BEAT_VERSIONS; do
	timeout 3h python consensus/simulate_consensus.py --consensus beat$v -f 1 -b $b --contacts_file=$CONTACTS_FILE0$s --iteration $ITERATION
	echo "DONE beat$v f=1 b=$b network=$CONTACTS_FILE0$s"
done
done

######################
# Dumbo1 simulations
######################

# Network: CONTACTS_FILE0
for b in $B; do
	timeout 3h python consensus/simulate_consensus.py --consensus dumbo1 -f 1 -k 2 -b $b --contacts_file=$CONTACTS_FILE0$s --iteration $ITERATION
	echo "DONE dumbo1 f=1 k=2 b=$b network=$CONTACTS_FILE0$s"
done

########################
## HBBFT simulations
########################

# Network: CONTACTS_FILE0
#for b in $B; do
#	timeout 5h python consensus/simulate_consensus.py --consensus hbbft -f 1 -b $b --contacts_file=$CONTACTS_FILE0$s --iteration $ITERATION
#	echo "DONE hbbft f=1 b=$b network=$CONTACTS_FILE0$s"
#done
done
