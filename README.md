== Dependencies & Installation

* [Charm](https://github.com/JHUISI/charm): It might be useful to follow this
  [guide](https://lrusso96.github.io/blog/cryptography/2021/03/04/charm-setup.html)

pip install requirements.txt
sudo apt-get install libmpc-dev 

== How to run?

* Tests: PYTHONPATH='.' pytest test/
* Simulation: PYTHONPATH='.' python consensus/simulate_consensus.py
