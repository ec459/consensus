import os
import re
import matplotlib.pyplot as plt
import numpy as np

filename_with_k_regex = r'(?P<c>[a-z0-9]+)_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_k(?P<k>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[' \
                        r'0-9]+)_seed(?P<seed>[0-9]+).log.avg_times'
filename_regex = r'(?P<c>[a-z0-9]+)_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[0-9]+)_seed(?P<seed>[0-9]+).log.avg_times'

log_files = [filename for filename in os.listdir(".") if filename.endswith('.log.avg_times')]

datas = {}

for filename in log_files:
    with open(filename, 'r') as f:
        latency, throughput, times, block_times = f.read().strip().split(' | ')
        latency, throughput = float(latency), float(throughput)
        times, block_times = eval(times), eval(block_times)

        if filename.startswith('dumbo1'):
            filename_regex_groups = re.search(filename_with_k_regex, filename).groupdict()
        else:
            filename_regex_groups = re.search(filename_regex, filename).groupdict()
        c, n = filename_regex_groups['c'], int(filename_regex_groups['n'])
        b, nw = int(filename_regex_groups['b']), int(filename_regex_groups['nw'])

        datas['{},{},{},{}'.format(c, n, b, nw)] = (latency, throughput, times, block_times)


for algo in ['hbbft', 'dumbo1', 'beat0', 'beat1']:
    for n in [4, 8, 12, 16]:
        for b in [100, 1000, 10000, 100000]:
            if all([('{},{},{},{}'.format(algo, n, b, i) in datas) for i in range(0, 3)]):
                print('{:7},n={},b={} | {:4.5f} | {:4.5f} | {:4.5f}'.format(
                    algo, n, b,
                    datas['{},{},{},{}'.format(algo, n, b, 0)][0],
                    datas['{},{},{},{}'.format(algo, n, b, 1)][0],
                    datas['{},{},{},{}'.format(algo, n, b, 2)][0]
                ))
            elif '{},{},{},{}'.format(algo, n, 100, 0) in datas and all(
                    [('{},{},{},{}'.format(algo, n, b, i) in datas) for i in range(1, 3)]):
                print('{:7},n={},b={} | {:4.5f} | {:4.5f} | {:4.5f}'.format(
                    algo, n, b,
                    datas['{},{},{},{}'.format(algo, n, 100, 0)][0],
                    datas['{},{},{},{}'.format(algo, n, b, 1)][0],
                    datas['{},{},{},{}'.format(algo, n, b, 2)][0]
                ))


# algos x b; nw = 0
# txs_per_min

def plot_avg_sim_time(b_size):
    labels = [4, 8, 12, 16]
    print(datas)

    def get_avg_and_err_of_algo_and_n(algo, n):
        algo_and_n_datas = list(filter(lambda v: v.split(',')[0] == algo and int(v.split(',')[1]) == n and b_size==int(v.split(',')[2]), datas))
        flatten_times = [v for l in algo_and_n_datas for v in datas[l][3]]
        return np.average(flatten_times), np.std(flatten_times)

    means_per_algo, err_per_algo = {}, {}
    for algo in ['hbbft', 'beat0', 'beat1', 'dumbo1']:
        avgs_and_errs = [get_avg_and_err_of_algo_and_n(algo, n) for n in labels]
        means_per_algo[algo] = [v[0] for v in avgs_and_errs]
        err_per_algo[algo] = [v[1] for v in avgs_and_errs]

    x = np.arange(len(labels))
    width = 0.2

    fig, ax = plt.subplots()
    ax.bar(x - 2 * width + width / 2, means_per_algo['hbbft'], width, yerr=err_per_algo['hbbft'], label='hbbft')
    ax.bar(x - width + width / 2, means_per_algo['beat0'], width, yerr=err_per_algo['beat0'], label='beat0')
    ax.bar(x + width - width / 2, means_per_algo['beat1'], width, yerr=err_per_algo['beat1'], label='beat1')
    ax.bar(x + 2 * width - width / 2, means_per_algo['dumbo1'], width, yerr=err_per_algo['dumbo1'], label='dumbo1')

    ax.set_ylabel('Duración [segundos]')
    ax.set_xlabel('Nodos')
    ax.set_title(f'Duración promedio de cada instancia de simulación B={b_size}')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    fig.tight_layout()
    plt.show()


def plot_latency(algo, n):
    labels = [100, 1000, 10000]

    def get_mean_and_avg_of_sim(algo, n, b, nw):
        sim_times = [np.average(v) for v in datas[f'{algo},{n},{b},{nw}'][2]]
        print(algo, n, b, nw, sim_times, np.average(sim_times), np.std(sim_times))
        return np.average(sim_times), np.std(sim_times)

    try:
        no_throughput_mean_and_err = [get_mean_and_avg_of_sim(algo, n, n, 0)] * len(labels)
    except:
        no_throughput_mean_and_err = [get_mean_and_avg_of_sim(algo, n, 100, 0)] * len(labels)
    networks_means = [[v[0] for v in no_throughput_mean_and_err], [], []]
    networks_err = [[v[1] for v in no_throughput_mean_and_err], [], []]

    for b in labels:
        for nw in [1, 2]:
            avg, err = get_mean_and_avg_of_sim(algo, n, b, nw)
            networks_means[nw].append(avg)
            networks_err[nw].append(err)

    fig, ax = plt.subplots()
    line_label = ['nw0: inf bps', 'nw1: 256kbps', 'nw2: 32kbps']
    for i in range(len(networks_means)):
        ax.errorbar(range(len(labels)), networks_means[i], yerr=networks_err[i], linestyle='dotted', label=line_label[i])

    ax.set_ylabel('Minutos')
    ax.set_xlabel('Transacciones por bloque')
    ax.set_title(f'Latencia: {algo} con {n} nodos')
    ax.set_xticks(range(len(labels)))
    ax.set_xticklabels(labels)
    ax.legend()
    fig.tight_layout()
    plt.show()


def plot_throughput(algo, n):
    labels = [100, 1000, 10000]

    def get_mean_and_avg_of_sim(algo, n, b, nw):
        sim_throughput = [b / np.average(v) for v in datas[f'{algo},{n},{b},{nw}'][2]]
        return np.average(sim_throughput), np.std(sim_throughput)

    networks_means = [[], []]
    networks_err = [[], []]

    for b in labels:
        for nw in [1, 2]:
            avg, err = get_mean_and_avg_of_sim(algo, n, b, nw)
            print(b, nw, avg, err)
            networks_means[nw-1].append(avg)
            networks_err[nw-1].append(err)

    fig, ax = plt.subplots()
    line_label = ['nw1: 256kbps', 'nw2: 32kbps']
    for i in range(len(networks_means)):
        ax.errorbar(range(len(labels)), networks_means[i], yerr=networks_err[i], linestyle='dotted', label=line_label[i])

    ax.set_ylabel('Transacciones por minuto')
    ax.set_xlabel('Transacciones por bloque')
    ax.set_title(f'Thoughput: {algo} con {n} nodos')
    ax.set_xticks(range(len(labels)))
    ax.set_xticklabels(labels)
    ax.legend()
    fig.tight_layout()
    plt.show()


plot_avg_sim_time(10000)
plot_latency('hbbft', 16)
plot_throughput('hbbft', 16)
