#!/usr/bin/bash

for i in $(seq 1 $ITERATION); do
  python beat_crypto/threshenc/generate_keys.py 14 4 >> "beat_simulation_n14_k4.setup"
  python beat_crypto/commoncoin/prf_generate_keys.py 14 4 >> "beat_simulation_n14_k4.setup"
done

# Network 0: no throughput
python simulate_consensus.py --consensus beat0 -n 14 -f 3 -b 14 --network 0 --seed 24

# Network 1, 2: 256kbps and 32kbps
for nw in {1,2}; do
for b in {100,250,500,750,1000,2500,3750,5000,6250,7500,8750,10000}; do
	python simulate_consensus.py --consensus beat0 -n 14 -f 3 -b $b --network $nw --seed 24
	echo "DONE beat0 n=14 f=3 b=$b network=$nw seed=24"
done
done
