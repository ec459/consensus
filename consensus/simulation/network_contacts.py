import copy
import csv
import dataclasses
from datetime import datetime, timedelta
import re
from typing import Union
import heapq
import networkx as nx

from consensus.simulation import network_delay

DATETIME_FORMAT = '%d %b %Y %H:%M:%S.%f'
DATETIME_PATTERN = re.compile("^[0-9]{1,2} [A-Z][a-z]{2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{3,6}$")


class ContactNetworkDelay(network_delay.NetworkDelay):
    """ Network delay class based on contacts. Assumes that asking for delay will be with non decreasing time. """

    def __init__(self, n, contacts_list, seed=None, f=None, scale=1):
        # defaulting f to be ceil(n / 4) as used in most simulations, also not used when calculating delay.
        if f is None:
            f = (n + 3) // 4
        super().__init__(n, f, seed)
        self.current_t = 0
        self.scale = scale
        self.contacts = self._build_contacts_from_contact_list(contacts_list)
        self.contacts_idx = [[0 for _ in range(n)] for _ in range(n)]
        self.throughput = 60 * 25 # bytes per minutes (25 bytes per sec) 

    def _build_contacts_from_contact_list(self, contacts_list):
        contacts = [[[] for _ in range(self.n)] for _ in range(self.n)]

        for contact in contacts_list:
            contacts[contact.source][contact.destination].append(contact)

        # sort all contacts by start time
        for i in range(self.n):
            for j in range(self.n):
                contacts[i][j] = sorted(contacts[i][j], key=lambda v: v.start)
                for k in range(len(contacts[i][j]) - 1):
                    assert (contacts[i][j][k].stop <= contacts[i][j][k + 1].start)

        return contacts

    def get_delay(self, i, j, t, msg_object):
        assert (self.current_t <= t)
        self.current_t = t

        if i == j:
            return 0

        contacts_i_j = self.contacts[i][j]
        idx = self.contacts_idx[i][j]
        # Get the first contact that ends on or after t. The packet/message will be sent during this contact.
        while idx < len(contacts_i_j) and contacts_i_j[idx].stop < t:
            idx += 1
        self.contacts_idx[i][j] = idx

        if idx < len(contacts_i_j) and t <= contacts_i_j[idx].stop:
            # The packet will be sent at the start of the (next) contact if there is no contact that contains t,
            # otherwise if there is a contact at t it will be sent at that moment.
            return max(0, contacts_i_j[idx].start - t) + contacts_i_j[idx].get_latency() * self.scale
            # + network_delay.get_message_length(msg_object) / self.throughput
        else:
            return -1


    def reset(self):
        self.current_t = 0
        self.contacts_idx = [[0 for _ in range(self.n)] for _ in range(self.n)]


@dataclasses.dataclass
class Contact:
    """Description of contact between two nodes.

    Attribute:
        source: index of source node
        destination: index of destination node
        start: start of contact in seconds
        stop: end of contact in seconds
        min_latency: minimum latency of sending the message
        max_latency: maximum latency of sending the message
    """
    source: int
    destination: int
    start: Union[int, datetime]
    stop: Union[int, datetime]
    min_latency: float
    max_latency: float

    @staticmethod
    def from_csv_row(node_to_idx, contact_with_str_values):
        assert DATETIME_PATTERN.match(contact_with_str_values[2])
        assert DATETIME_PATTERN.match(contact_with_str_values[3])

        return Contact(
            source=node_to_idx[contact_with_str_values[0]],
            destination=node_to_idx[contact_with_str_values[1]],
            start=datetime.strptime(contact_with_str_values[2], DATETIME_FORMAT),
            stop=datetime.strptime(contact_with_str_values[3], DATETIME_FORMAT),
            min_latency=int(contact_with_str_values[4]) / 60.,
            max_latency=int(contact_with_str_values[5]) / 60.
        )

    def get_latency(self):
        return (self.min_latency + self.max_latency) / 2.


def parse_csv_to_network_delay(csv_path):
    """Parses the network contacts csv to network delay.

    The csv has the following columns: Source, Destination, StartTime[UTCG], StopTime[UTCG], MinLatency[s],
    MaxLatency[s].

    Args:
        csv_path: CSV path of network contacts.
    """
    with open(csv_path, 'r') as csv_file:
        # Skipping header row
        contacts_csv_rows = list(csv.reader(csv_file))[1:]
        # Union of source and destination nodes
        nodes = sorted(list(set([c[0] for c in contacts_csv_rows] + [c[1] for c in contacts_csv_rows])))

    node_to_idx = {node_name: idx for idx, node_name in enumerate(nodes)}
    contacts = list(map(lambda v: Contact.from_csv_row(node_to_idx, v), contacts_csv_rows))
    print(node_to_idx)
    # Offset start and stop datetime so minimum is 0 and then express it in minutes (decimal) since min_datetime
    min_datetime = min([min(c.start, c.stop) for c in contacts])
    max_datetime = max([max(c.start, c.stop) for c in contacts])

    def offset_datetime_of_contact_and_express_it_in_minutes(contact):
        offsetted_contact = copy.deepcopy(contact)
        offsetted_contact.start = float((offsetted_contact.start - min_datetime) / timedelta(seconds=60))
        offsetted_contact.stop = float((offsetted_contact.stop - min_datetime) / timedelta(seconds=60))
        return offsetted_contact

    contacts = list(map(offset_datetime_of_contact_and_express_it_in_minutes, contacts))

    return ContactNetworkDelay(len(nodes), contacts)
