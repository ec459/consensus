import abc
import logging
import pickle
import random

logger = logging.getLogger(__name__)


class NetworkDelay(metaclass=abc.ABCMeta):
    def __init__(self, n, f, seed):
        self.n = n
        self.f = f
        self.seed = seed if seed is not None else random.Random(None).getrandbits(32)
        logger.info(f'init network: n={n} f={f} seed={seed}')

    @abc.abstractmethod
    def get_delay(self, _i, _j, _t, _msg_obj):
        pass

    @abc.abstractmethod
    def reset(self):
        pass


###############################################################################
# Helper and utilities functions for creating some sample networks
###############################################################################


def simple_random_latency(i, j, t, n, seed):
    latency_seed = i + n * j + n ** 2 * (int(t // 15) + seed)
    return random.Random(latency_seed).random() * 30


def get_message_length(msg_object):
    try:
        msg_str = pickle.dumps(msg_object)
    except:
        msg_str = pickle.dumps(str(msg_object))
    return len(msg_str)


###############################################################################
# Sample networks constructors
###############################################################################


class SimpleDelayWithoutThroughput(NetworkDelay):
    def get_delay(self, i, j, t, _msg_object):
        return simple_random_latency(i, j, t, self.n, self.seed)

    def reset(self):
        pass


class SimpleDelayWithConstantThroughput(NetworkDelay):
    def __init__(self, n, f, seed, throughput_bps):
        super().__init__(n, f, seed)
        # Throughput expressed in bytes per minutes
        self.throughput = (throughput_bps / 8.) * 60

    def get_delay(self, i, j, t, msg_object):
        return simple_random_latency(i, j, t, self.n, self.seed) + get_message_length(msg_object) / self.throughput

    def reset(self):
        pass
