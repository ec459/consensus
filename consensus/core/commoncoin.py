# based on
from collections import defaultdict
import hashlib
import logging

from simpy.resources.store import Store

from consensus.crypto.threshsig import boldyreva
from consensus.beat_crypto.commoncoin import thresprf

logger = logging.getLogger(__name__)


class CommonCoinFailureException(Exception):
    """Raised for common coin failures."""
    pass


def hash(x):
    return hashlib.sha256(x).digest()


def compute_output_bit_boldyreva(hashed_sig):
    """Get a bit of hashed signature given in bytes.
    """
    return boldyreva.serialize(hashed_sig)[0] % 2


def compute_output_bit_beat(hashed_sig):
    """Get a bit of hashed signature given in bytes.
    """
    return thresprf.serialize(hashed_sig)[0] % 2


def compute_output_to_int(hashed_sig):
    """Convert hashed signature given in bytes to int.
    """
    return int.from_bytes(boldyreva.serialize(hashed_sig), 'big')


def shared_coin(sid, pid, N, f, PK, SK, broadcast, receive, env, compute_output=compute_output_bit_boldyreva):
    """A shared coin based on threshold signatures

    :param sid: a unique instance id
    :param pid: my id number
    :param N: number of parties
    :param f: fault tolerance, :math:`f+1` shares needed to get the coin
    :param PK: ``boldyreva.TBLSPublicKey`` or ``TPRFPublicKeyWithGG``
    :param SK: ``boldyreva.TBLSPrivateKey`` or ``TPRFPrivateKeyWithGG``
    :param broadcast: broadcast channel
    :param receive: receive channel
    :param env: simpy environment for simulation
    :param compute_output: compute the output from hashed signature
    :return: a function ``getCoin()``, where ``getCoin(r)`` blocks
    """
    assert PK.k == f + 1
    assert PK.l == N
    received = defaultdict(dict)
    outputQueue = defaultdict(lambda: Store(env, capacity=1))

    def _recv():
        while True:  # main receive loop
            # logger.debug(f'entering loop',
            #             extra={'nodeid': pid, 'epoch': '?'})
            # New shares for some round r, from sender i
            (i, (_, r, sig)) = yield receive()
            #logger.debug(f'received i, _, r, sig: {i, _, r, sig}',
            #             extra={'nodeid': pid, 'epoch': r})
            assert i in range(N)
            assert r >= 0
            if i in received[r]:
                print("redundant coin sig received", (sid, pid, i, r))
                continue

            h = PK.hash_message(str((sid, r)))

            try:
                PK.verify_share(sig, i, h)
            except AssertionError:
                print("Signature share failed!", (sid, pid, i, r))
                continue

            received[r][i] = sig

            # After reaching the threshold, compute the output and
            # make it available locally
            # logger.debug(
            #    f'if len(received[r]) == f + 1: {len(received[r]) == f + 1}',
            #    extra={'nodeid': pid, 'epoch': r},
            # )
            if len(received[r]) == f + 1:
                # Verify and get the combined signature
                sigs = dict(list(received[r].items())[:f + 1])
                sig = PK.combine_shares(sigs)
                assert PK.verify_signature(sig, h)

                output = compute_output(sig)
                # logger.debug(f'put {output} in output queue',
                #             extra={'nodeid': pid, 'epoch': r})
                outputQueue[r].put(output)

    env.process(_recv())

    def get_coin(round):
        """Gets a coin.

        :param round: the epoch/round.
        :returns: a coin.
        """
        h = PK.hash_message(str((sid, round)))
        # logger.debug(f"broadcast {('COIN', round, SK.sign(h))}",
        #             extra={'nodeid': pid, 'epoch': round})
        broadcast(('COIN', round, SK.sign(h)))
        coin = yield outputQueue[round].get()
        return coin

    return get_coin
