from collections import defaultdict

global msg_sent
msg_sent = 0

global aba_rounds
aba_rounds = defaultdict(lambda: defaultdict(int))

global block_creations
block_creations = defaultdict(int)