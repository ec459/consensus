from collections import defaultdict


def reliablebroadcast(sid, pid, N, f, leader, input, receive, send):
    """Reliable broadcast

    :param int pid: ``0 <= pid < N``
    :param int N:  at least 3
    :param int f: fault tolerance, ``N >= 3f + 1``
    :param int leader: ``0 <= leader < N``
    :param input: if ``pid == leader``, then :func:`input()` is called
        to wait for the input value
    :param receive: :func:`receive()` blocks until a message is
        received; message is of the form::

            (i, (tag, ...)) = receive()

        where ``tag`` is one of ``{"VAL", "ECHO", "READY"}``
    :param send: sends (without blocking) a message to a designed
        recipient ``send(i, (tag, ...))``

    :return str: ``m`` after receiving :math:`2f+1` ``READY`` messages
        and :math:`N-2f` ``ECHO`` messages

        .. important:: **Messages**

            ``VAL( roothash, branch[i], stripe[i] )``
                sent from ``leader`` to each other party
            ``ECHO( roothash, branch[i], stripe[i] )``
                sent after receiving ``VAL`` message
            ``READY( roothash )``
                sent after receiving :math:`N-f` ``ECHO`` messages
                or after receiving :math:`f+1` ``READY`` messages

    .. todo::
        **Accountability**

        A large computational expense occurs when attempting to
        decode the value from erasure codes, and recomputing to check it
        is formed correctly. By transmitting a signature along with
        ``VAL`` and ``ECHO``, we can ensure that if the value is decoded
        but not necessarily reconstructed, then evidence incriminates
        the leader.

    """
    assert N >= 3 * f + 1
    assert f >= 0
    assert 0 <= pid < N

    echo_threshold_for_voting = ((N+f+1) + 1) // 2

    def broadcast(o):
        for i in range(N):
            send(i, o)

    if not isinstance(leader, int):
        leader = yield leader.get()

    assert 0 <= leader < N

    if pid == leader:
        m = yield input()  # block until an input is received
        assert isinstance(m, (str, bytes))
        broadcast(('SEND', m))

    values_count = defaultdict(int)
    send_received = False
    echo_senders = set()  # Peers that have sent us ECHO messages
    vote_senders = set()  # Peers that have sent us READY messages
    vote_counts = defaultdict(int)
    vote_sent = False

    while True:  # main receive loop
        sender, msg = yield receive()
        if msg[0] == 'SEND' and sender == leader:
            (_, value) = msg
            if not send_received:
                broadcast(('ECHO', value))
                send_received = True
        elif msg[0] == 'ECHO':
            (_, value) = msg

            if sender not in echo_senders:
                echo_senders.add(sender)
                values_count[value] += 1
                if values_count[value] >= echo_threshold_for_voting and not vote_sent:
                    broadcast(('VOTE', value))
                    vote_sent = True
        elif msg[0] == 'VOTE':
            (_, value) = msg

            if sender in vote_senders:
                continue

            vote_senders.add(sender)
            vote_counts[value] += 1

            if vote_counts[value] >= f + 1 and not vote_sent:
                broadcast(('VOTE', value))
                vote_sent = True

            if vote_counts[value] >= 2 * f + 1:
                return value
