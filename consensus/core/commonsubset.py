# Based on https://github.com/initc3/HoneyBadgerBFT-Python/blob/dev/honeybadgerbft/core/commonsubset.py
import simpy


def commonsubset(pid, N, f, rbc_out, aba_in, aba_out, env):
    """The BKR93 algorithm for asynchronous common subset.

    :param pid: my identifier
    :param N: number of nodes
    :param f: fault tolerance
    :param rbc_out: an array of :math:`N` (blocking) output functions,
        returning a string
    :param aba_in: an array of :math:`N` (non-blocking) functions that
        accept an input bit
    :param aba_out: an array of :math:`N` (blocking) output functions,
        returning a bit
    :return: an :math:`N`-element array, each element either ``None`` or a
        string
    """
    assert len(rbc_out) == N
    assert len(aba_in) == N
    assert len(aba_out) == N

    aba_inputted = [False] * N
    aba_values = [0] * N
    rbc_values = [None] * N

    def _recv_rbc(j):
        try:
            # Receive output from reliable broadcast
            rbc_values[j] = yield env.process(rbc_out[j]())
            if not aba_inputted[j]:
                # Provide 1 as input to the corresponding bin agreement
                aba_inputted[j] = True
                aba_in[j](1)
        except simpy.Interrupt as i:
            pass

    r_threads = [env.process(_recv_rbc(j)) for j in range(N)]

    def _recv_aba(j):
        # Receive output from binary agreement
        aba_values[j] = yield aba_out[j]()  # May block
        # print pid, j, 'ENTERING CRITICAL'
        if sum(aba_values) >= N - f:
            # Provide 0 to all other aba
            for k in range(N):
                if not aba_inputted[k]:
                    aba_inputted[k] = True
                    aba_in[k](0)

    # Wait for all binary agreements
    a_threads = [env.process(_recv_aba(j)) for j in range(N)]
    yield env.all_of(a_threads)

    assert sum(aba_values) >= N - f  # Must have at least N-f committed

    # Wait for the corresponding broadcasts
    for j in range(N):
        if aba_values[j]:
            yield env.all_of([r_threads[j]])
            assert rbc_values[j] is not None
        else:
            r_threads[j].interrupt()
            rbc_values[j] = None

    return tuple(rbc_values)
