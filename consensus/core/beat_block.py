import os
import logging

from consensus.beat_crypto.threshenc.tdh2 import serialize, serialize1, deserialize, deserialize0, deserialize1, \
    deserialize2, TDHPublicKey, TDHPrivateKey, group, encrypt, decrypt
import consensus.core.vars

logger = logging.getLogger(__name__)

# based on BEAT0/BEAT/core/utils.py
EC_SERIALIZED_1 = 32
EC_SERIALIZED_2 = 46
EC_LABEL_LENGTH = 1
ENC_SERIALIZED_LENGTH = EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2 * 4


# based on BEAT0/BEAT/core/utils.py
def serialize_tkey(C):
    assert len(C[0]) == EC_SERIALIZED_1
    assert len(C[1]) == EC_LABEL_LENGTH
    assert len(serialize1(C[2])) == EC_SERIALIZED_2
    assert len(serialize1(C[3])) == EC_SERIALIZED_2
    assert len(serialize1(C[4])) == EC_SERIALIZED_2
    assert len(serialize1(C[5])) == EC_SERIALIZED_2
    return b''.join((C[0], C[1], serialize1(C[2]), serialize1(C[3]), serialize1(C[4]), serialize1(C[5])))


# based on BEAT0/BEAT/core/utils.py
def deserialize_tkey(r):
    return (r[:EC_SERIALIZED_1], r[EC_SERIALIZED_1:EC_SERIALIZED_1 + EC_LABEL_LENGTH],
            deserialize(r[EC_SERIALIZED_1 + EC_LABEL_LENGTH: EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2]),
            deserialize(r[
                        EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2:EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2 * 2]),
            deserialize(r[
                        EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2 * 2:EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2 * 3]),
            deserialize(r[EC_SERIALIZED_1 + EC_LABEL_LENGTH + EC_SERIALIZED_2 * 3:ENC_SERIALIZED_LENGTH]))


# Based on honeybadger_block.py
def beat_block(sid, pid, N, f, PK, SK, propose_in, acs_in, acs_out, tpke_bcast, tpke_recv, env):
    """The BEAT algorithm for a single block

    :param pid: my identifier
    :param N: number of nodes
    :param f: fault tolerance
    :param PK: threshold encryption public key
    :param SK: threshold encryption secret key
    :param propose_in: a function returning a sequence of transactions
    :param acs_in: a function to provide input to acs routine
    :param acs_out: a blocking function that returns an array of ciphertexts
    :param tpke_bcast:
    :param tpke_recv:
    :return:
    """

    # Broadcast inputs are of the form (tenc(key), enc(key, transactions))

    # Threshold encrypt
    prop = yield propose_in()
    key = os.urandom(32)  # random 256-bit key
    ciphertext = encrypt(key, prop.encode())
    tkey = PK.encrypt(key, "1".encode())

    import pickle
    to_acs = pickle.dumps((serialize_tkey(tkey), ciphertext))
    acs_in(to_acs)

    # Wait for the corresponding ACS to finish
    yield env.all_of([acs_out])
    vall = acs_out.value

    assert len(vall) == N
    assert len([_ for _ in vall if _ is not None]) >= N - f  # This many must succeed

    # Broadcast all our decryption shares
    my_shares = []
    for i, v in enumerate(vall):
        if v is None:
            my_shares.append(None)
            continue
        (tkey, ciph) = pickle.loads(v)
        tkey = deserialize_tkey(tkey)
        share = SK.decrypt_share(tkey)
        my_shares.append(share)

    tpke_bcast(my_shares)

    # Receive everyone's shares
    shares_received = {}
    while len(shares_received) < f + 1:
        (j, shares) = yield tpke_recv()
        if j in shares_received:
            # TODO: alert that we received a duplicate
            print('Received a duplicate decryption share from', j)
            continue
        shares_received[j] = shares

    assert len(shares_received) >= f + 1

    decryptions = []
    for i, v in enumerate(vall):
        if v is None:
            continue
        svec = {}
        for j, shares in shares_received.items():
            svec[j] = shares[i]  # Party j's share of broadcast i
        (tkey, ciph) = pickle.loads(v)
        tkey = deserialize_tkey(tkey)
        key = PK.combine_shares(tkey, svec)
        plain = decrypt(key, ciph)
        decryptions.append(plain)

    logger.info(f'<BLOCK CREATED> Node {pid} at {env.now} with_msg_sent={consensus.core.vars.msg_sent}')

    consensus.core.vars.block_creations[pid] = env.now

    return tuple(decryptions)
