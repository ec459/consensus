# based on https://github.com/initc3/HoneyBadgerBFT-Python/blob/dev/honeybadgerbft/core/commonsubset.py
# and adapting it for Dumbo ACS
import simpy


def commonsubset(pid, N, K, txs_rbc_out, rbc_out, aba_in, aba_out, env):
    """The BKR93 algorithm for asynchronous common subset.

    :param pid: my identifier
    :param N: number of nodes
    :param f: fault tolerance
    :param rbc_out: an array of :math:`N` (blocking) output functions,
        returning a string
    :param aba_in: an array of :math:`N` (non-blocking) functions that
        accept an input bit
    :param aba_out: an array of :math:`N` (blocking) output functions,
        returning a bit
    :return: an :math:`N`-element array, each element either ``None`` or a
        string
    """
    assert len(rbc_out) == K
    assert len(aba_in) == K
    assert len(aba_out) == K

    aba_inputted = [False] * K
    aba_values = [0] * K
    rbc_values = [None] * K

    txs_rbc_values = [0] * N
    txs_rbc_outputted = [False] * N

    def _recv_txs_rbc(j):
        if not txs_rbc_outputted[j]:
            txs_rbc_values[j] = yield env.process(txs_rbc_out[j]())
            txs_rbc_outputted[j] = True
        return txs_rbc_values[j]

    def _recv_rbc(j):
        try:
            # Receive output from reliable broadcast
            rbc_values[j] = yield env.process(rbc_out[j]())
            if not aba_inputted[j]:
                # Make sure all corresponding txs arrived and
                # Provide 1 as input to the corresponding bin agreement
                for i in range(N):
                    if rbc_values[j][i] == '1':
                        txs_rbc_values[i] = yield env.process(_recv_txs_rbc(i))

                aba_inputted[j] = True
                aba_in[j](1)
        except simpy.Interrupt as i:
            pass

    r_threads = [env.process(_recv_rbc(j)) for j in range(K)]

    def _recv_aba(j):
        # Receive output from binary agreement
        aba_values[j] = yield aba_out[j]()  # May block
        # Provide 0 to all other aba
        for k in range(K):
            if not aba_inputted[k]:
                aba_inputted[k] = True
                aba_in[k](0)

    # Wait for all binary agreements
    a_threads = [env.process(_recv_aba(j)) for j in range(K)]
    yield env.all_of(a_threads)

    # Calculate union of bitset from leaders
    txs_idx_bitset = 0
    for j in range(K):
        if aba_values[j]:
            yield env.all_of([r_threads[j]])
            assert rbc_values[j] is not None
            txs_idx_bitset = txs_idx_bitset | int(rbc_values[j][::-1], 2)
        else:
            if not r_threads[j].processed:
                r_threads[j].interrupt()
            rbc_values[j] = None

    # Convert bitset to string and add padding
    txs_idx_bitset = bin(txs_idx_bitset)[2:][::-1]
    txs_idx_bitset = txs_idx_bitset + "0" * (N - len(txs_idx_bitset))

    # Wait for the corresponding broadcasts
    for j in range(N):
        if txs_idx_bitset[j] == '1':
            txs_rbc_values[j] = yield env.process(_recv_txs_rbc(j))
            assert txs_rbc_values[j] is not None
        else:
            txs_rbc_values[j] = None

    return tuple(txs_rbc_values)
