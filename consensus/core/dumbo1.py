from collections import namedtuple
from enum import Enum
import logging
from random import Random

import simpy
from simpy.resources.store import Store
from simpy.events import Condition

from consensus.core.binaryagreement import binaryagreement
from consensus.core.commoncoin import shared_coin, compute_output_to_int
from consensus.core.commonsubset_dumbo import commonsubset
from consensus.core.honeybadger_block import honeybadger_block
from consensus.core.reliablebroadcast import reliablebroadcast
from consensus.exceptions import UnknownTagError

logger = logging.getLogger(__name__)


class BroadcastTag(Enum):
    ACS_COIN = 'ACS_COIN'
    ACS_RBC = 'ACS_RBC'
    ACS_ABA = 'ACS_ABA'
    INDEX_COIN = 'INDEX_COIN'
    INPUT_RBC = 'INPUT_RBC'
    TPKE = 'TPKE'


BroadcastReceiverQueues = namedtuple(
    'BroadcastReceiverQueues', ('ACS_COIN', 'ACS_ABA', 'ACS_RBC', 'INDEX_COIN', 'INPUT_RBC', 'TPKE'))


def broadcast_receiver(recv_func, recv_queues):
    sender, (tag, j, msg) = yield recv_func()
    if tag not in BroadcastTag.__members__:
        raise UnknownTagError('Unknown tag: {}! Must be one of {}.'.format(tag, BroadcastTag.__members__.keys()))
    recv_queue = recv_queues._asdict()[tag]

    if tag not in [BroadcastTag.TPKE.value, BroadcastTag.INDEX_COIN.value]:
        recv_queue = recv_queue[j]

    recv_queue.put((sender, msg))


def broadcast_receiver_loop(recv_func, recv_queues, env):
    while True:
        yield env.process(broadcast_receiver(recv_func, recv_queues))


class Dumbo1:
    r"""HoneyBadgerBFT object used to run the protocol.

    :param str sid: The base name of the common coin that will be used to
        derive a nonce to uniquely identify the coin.
    :param int pid: Node id.
    :param int B: Batch size of transactions.
    :param int N: Number of nodes in the network.
    :param int f: Number of faulty nodes that can be tolerated.
    :param str sPK: Public key of the threshold signature
        (:math:`\mathsf{TSIG}`) scheme.
    :param str sSK: Signing key of the threshold signature
        (:math:`\mathsf{TSIG}`) scheme.
    :param str ePK: Public key of the threshold encryption
        (:math:`\mathsf{TPKE}`) scheme.
    :param str eSK: Signing key of the threshold encryption
        (:math:`\mathsf{TPKE}`) scheme.
    :param send:
    :param recv:
    """

    def __init__(self, sid, pid, B, N, K, f, sPK, sSK, ePK, eSK, send, recv, env):
        self.sid = sid
        self.pid = pid
        self.B = B
        self.N = N
        self.K = K
        self.f = f
        self.sPK = sPK
        self.sSK = sSK
        self.ePK = ePK
        self.eSK = eSK
        self._send = send
        self._recv = recv
        self._env = env

        self.round = 0  # Current block number
        self.transaction_buffer = []
        self._per_round_recv = {}  # Buffer of incoming messages

    def submit_tx(self, tx):
        """Appends the given transaction to the transaction buffer.

        :param tx: Transaction to append to the buffer.
        """
        self.transaction_buffer.append(tx)

    def run(self):
        """Run the HoneyBadgerBFT protocol."""

        def _recv():
            """Receive messages."""
            while True:
                (sender, (r, msg)) = yield self._recv()
                # Maintain an *unbounded* recv queue for each epoch
                if r not in self._per_round_recv:
                    # Buffer this message
                    assert r >= self.round  # pragma: no cover
                    self._per_round_recv[r] = simpy.resources.store.Store(self._env)

                _recv = self._per_round_recv[r]
                if _recv is not None:
                    # Queue it
                    _recv.put((sender, msg))

                # else:
                # We have already closed this
                # round and will stop participating!

        self._recv_thread = self._env.process(_recv())

        while True:
            # For each round...
            r = self.round
            if r not in self._per_round_recv:
                self._per_round_recv[r] = Store(self._env)

            tx_to_send = self.transaction_buffer[:self.B]

            # Run the round
            def _make_send(r):
                def _send(j, o):
                    self._send(j, (r, o))

                return _send

            send_r = _make_send(r)
            recv_r = self._per_round_recv[r].get
            new_tx = yield self._run_round(r, str(tx_to_send), send_r, recv_r)

            # Remove all of the new transactions from the buffer
            self.transaction_buffer = [_tx for _tx in self.transaction_buffer if _tx not in new_tx]

            self.round += 1  # Increment the round
            if self.round >= 1:
                return new_tx  # Only run one round for now

    def _run_round(self, r, tx_to_send, send, recv):
        """Run one protocol round.

        :param int r: round id
        :param tx_to_send: Transaction(s) to process.
        :param send:
        :param recv:
        """
        # Unique sid for each round
        sid = self.sid + ':' + str(r)
        pid = self.pid

        def broadcast(o):
            for j in range(self.N):
                send(j, o)

        index_coin_recv = Store(self._env)

        txs_rbcs = [None] * self.N
        input_rbc_recvs = [None] * self.N
        input_rbc_outputs = [Store(self._env, capacity=1) for _ in range(self.N)]
        tx_rbc_input = Store(self._env, capacity=1)

        leaders_recvs = [Store(self._env, capacity=1) for _ in range(self.K)]

        coin_recvs = [None] * self.K
        aba_recvs = [None] * self.K
        rbc_recvs = [None] * self.K

        aba_inputs = [Store(self._env, capacity=1) for _ in range(self.K)]
        aba_outputs = [Store(self._env, capacity=1) for _ in range(self.K)]
        rbc_outputs = [Store(self._env, capacity=1) for _ in range(self.K)]

        def setup_input_rbc(j):
            def rbc_send(k, o):
                send(k, ('INPUT_RBC', j, o))

            input_rbc_recvs[j] = Store(self._env)

            # Only leader gets input
            rbc_input = tx_rbc_input.get if j == pid else None
            txs_rbcs[j] = self._env.process(reliablebroadcast(
                sid + 'INPUT_RBC' + str(j), pid, self.N, self.f,
                j, rbc_input, input_rbc_recvs[j].get, rbc_send))

            def rbc_get():
                yield self._env.all_of([txs_rbcs[j]])
                return txs_rbcs[j].value

            input_rbc_outputs[j] = rbc_get  # block for output from rbc

        def setup_leaders_indexes():
            def coin_bcast(o):
                broadcast(('INDEX_COIN', self.round, o))

            coin = shared_coin(sid + 'INDEX_COIN', pid, self.N, self.f,
                               self.sPK, self.sSK, coin_bcast,
                               index_coin_recv.get, self._env, compute_output_to_int)

            def fill_leaders():
                leaders_seed = yield self._env.process(coin(self.round))
                leaders = Random(leaders_seed).sample(range(self.N), self.K)
                # print(pid, 'got leaders at', self._env.now)
                for i in range(self.K):
                    leaders_recvs[i].put(leaders[i])

            self._env.process(fill_leaders())

        def setup_leaders(j):
            def coin_bcast(o):
                broadcast(('ACS_COIN', j, o))

            coin_recvs[j] = Store(self._env)
            coin = shared_coin(sid + 'COIN' + str(j), pid, self.N, self.f,
                               self.sPK, self.sSK, coin_bcast,
                               coin_recvs[j].get, self._env)

            def aba_bcast(o):
                broadcast(('ACS_ABA', j, o))

            aba_recvs[j] = Store(self._env)
            self._env.process(binaryagreement(
                sid + 'ABA' + str(j), pid, self.N, self.f, coin, aba_inputs[j].get,
                aba_outputs[j].put, aba_bcast, aba_recvs[j].get, self._env))

            def rbc_send(k, o):
                send(k, ('ACS_RBC', j, o))

            index_input_buffer = Store(self._env, capacity=1)
            leader_buffer = Store(self._env, capacity=1)

            def rbc_input():
                leader = yield leaders_recvs[j].get()
                leader_buffer.put(leader)
                if leader == pid:
                    yield Condition(self._env, lambda e, p: p >= self.N - self.f, txs_rbcs)
                    bitset = 0
                    for idx, e in enumerate(txs_rbcs):
                        if e.processed:
                            bitset = bitset | (1 << idx)
                    # return string where pos i, indicates if txs from node i is included
                    val = bin(bitset)[2:][::-1]
                    val = val + "0" * (self.N - len(val))
                    logger.debug(f'<LEADER> {leader} proposal = {val} at {self._env.now}')
                    index_input_buffer.put(val)

            self._env.process(rbc_input())
            rbc_input = index_input_buffer.get
            rbc_recvs[j] = Store(self._env)
            rbc = self._env.process(reliablebroadcast(
                sid + 'RBC' + str(j), pid, self.N, self.f, leader_buffer, rbc_input,
                rbc_recvs[j].get, rbc_send))

            def rbc_get():
                yield self._env.all_of([rbc])
                return rbc.value

            rbc_outputs[j] = rbc_get  # block for output from rbc

        # N instances of transactions RBC
        for j in range(self.N):
            setup_input_rbc(j)

        # Pick leaders from nodes
        setup_leaders_indexes()

        # Setup RBC and ABA for each leader
        for j in range(self.K):
            setup_leaders(j)

        # Setup TPKE
        def tpke_bcast(o):
            broadcast(('TPKE', 0, o))

        tpke_recv = Store(self._env)

        acs = self._env.process(commonsubset(
            pid, self.N, self.K, input_rbc_outputs, rbc_outputs,
            [_.put for _ in aba_inputs],
            [_.get for _ in aba_outputs], self._env))

        recv_queues = BroadcastReceiverQueues(
            ACS_COIN=coin_recvs,
            ACS_ABA=aba_recvs,
            ACS_RBC=rbc_recvs,
            TPKE=tpke_recv,
            INPUT_RBC=input_rbc_recvs,
            INDEX_COIN=index_coin_recv
        )
        self._env.process(broadcast_receiver_loop(recv, recv_queues, self._env))

        _input = Store(self._env, capacity=1)
        _input.put(tx_to_send)
        return self._env.process(honeybadger_block(pid, self.N, self.f, self.ePK, self.eSK,
                                                   _input.get, acs_in=tx_rbc_input.put, acs_out=acs,
                                                   tpke_bcast=tpke_bcast, tpke_recv=tpke_recv.get, env=self._env))
