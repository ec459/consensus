# Based on https://github.com/initc3/HoneyBadgerBFT-Python/blob/dev/honeybadgerbft/core/honeybadger.py
import os
import logging

from consensus.crypto.threshenc import tpke
import consensus.core.vars

logger = logging.getLogger(__name__)


def serialize_uvw(u, v, w):
    # U: element of g1 (65 byte serialized for SS512)
    u = tpke.serialize(u)
    assert len(u) == 65
    # V: 32 byte str
    assert len(v) == 32
    # W: element of g2 (32 byte serialized for SS512)
    w = tpke.serialize(w)
    assert len(w) == 65
    return u, v, w


def deserialize_uvw(u, v, w):
    assert len(u) == 65
    assert len(v) == 32
    assert len(w) == 65
    u = tpke.deserialize1(u)
    w = tpke.deserialize2(w)
    return u, v, w


def honeybadger_block(pid, n, f, PK, SK, propose_in, acs_in, acs_out, tpke_bcast, tpke_recv, env):
    """The HoneyBadgerBFT algorithm for a single block

    :param pid: my identifier
    :param n: number of nodes
    :param f: fault tolerance
    :param PK: threshold encryption public key
    :param SK: threshold encryption secret key
    :param propose_in: a function returning a sequence of transactions
    :param acs_in: a function to provide input to acs routine
    :param acs_out: a blocking function that returns an array of ciphertexts
    :param tpke_bcast:
    :param tpke_recv:
    :return:
    """

    # Broadcast inputs are of the form (tenc(key), enc(key, transactions))

    # Threshold encrypt
    prop = yield propose_in()
    key = os.urandom(32)  # random 256-bit key
    ciphertext = tpke.encrypt(key, prop)
    tkey = PK.encrypt(key)

    import pickle
    to_acs = pickle.dumps((serialize_uvw(*tkey), ciphertext))
    acs_in(to_acs)

    # Wait for the corresponding ACS to finish
    yield env.all_of([acs_out])
    vall = acs_out.value

    assert len(vall) == n
    assert len([_ for _ in vall if _ is not None]) >= n - f  # This many must succeed

    # Broadcast all our decryption shares
    my_shares = []
    for i, v in enumerate(vall):
        if v is None:
            my_shares.append(None)
            continue
        (tkey, ciph) = pickle.loads(v)
        tkey = deserialize_uvw(*tkey)
        share = SK.decrypt_share(*tkey)
        # share is of the form: U_i, an element of group1
        my_shares.append(share)

    tpke_bcast(my_shares)

    # Receive everyone's shares
    shares_received = {}
    while len(shares_received) < f + 1:
        (j, shares) = yield tpke_recv()
        if j in shares_received:
            logger.warning(f'Received a duplicate decryption share from {j}')
            continue
        shares_received[j] = shares

    assert len(shares_received) >= f + 1

    decryptions = []
    for i, v in enumerate(vall):
        if v is None:
            continue
        svec = {}
        for j, shares in shares_received.items():
            svec[j] = shares[i]  # Party j's share of broadcast i
        (tkey, ciph) = pickle.loads(v)
        tkey = deserialize_uvw(*tkey)
        key = PK.combine_shares(*tkey, svec)
        plain = tpke.decrypt(key, ciph)
        decryptions.append(plain)

    logger.info(f'<BLOCK CREATED> Node {pid} at {env.now} with_msg_sent={consensus.core.vars.msg_sent}')

    consensus.core.vars.block_creations[pid] = env.now

    return tuple(decryptions)
