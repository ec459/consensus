import os
import re
import sys
import numpy as np

filename_with_k_regex = r'[a-z0-9]+_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_k(?P<k>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[' \
                        r'0-9a-z_A-Z-]+)(|_seed(?P<seed>[0-9]+)).log'
filename_regex = r'[a-z0-9]+_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[0-9a-zA-Z_-]+)(|_seed(?P<seed>[0-9]+)).log'

LATEX_TABLE_HEADER = 'Algoritmo & Promedio & Desvío & Mediana & Min & Max & Iteraciones \\\\'
LATEX_TABLE_ROW_FORMAT = '{algorithm} & {avg:.2f} & {std:.2f} & {median:.2f} & {min:.2f} & {max:.2f} & {iter} \\\\'

def print_latex_row(filepath, data):
    print(LATEX_TABLE_ROW_FORMAT.format(algorithm = filepath[:filepath.find('_')].upper(), avg = np.mean(data), std = np.std(data), median = np.median(data), min = min(data), max = max(data), iter = len(data)))


def get_information_from_logs(filepath):
    with open(filepath, 'r') as f:
        # line = {LEVEL}:{FUNCTION}:{MESSAGE}
        lines = [line.split(':')[2] for line in f.readlines()]
        
        # filter only block creation messages
        times = list(filter(lambda m: m.startswith('<TIME>'), lines))
        times = list(map(lambda m: float(m.split()[-1]), times))
        lines = list(filter(lambda m: m.startswith('<BLOCK CREATED>'), lines))

    filename = os.path.basename(filepath)

    block_creation_regex = r'<BLOCK CREATED> Node [0-9]+ at ([0-9]+.[0-9]+) with_msg_sent=[0-9]+'
    sim_time_regex = r'<TIME> Simulation duration = ([0-9]+.[0-9]+)'

    if filename.startswith('dumbo1'):
        filename_regex_groups = re.search(filename_with_k_regex, filename).groupdict()
    else:
        filename_regex_groups = re.search(filename_regex, filename).groupdict()

    n = int(filename_regex_groups['n'])
    if len(lines) % n != 0:
        lines = lines [: (len(lines) // n) * n]
    assert (len(lines) % n == 0)

    finish_times_flatten = [
            float(re.search(block_creation_regex, line).group(1)) / 60.
            for line in lines
    ]
    block_creation_avg_time = np.mean(finish_times_flatten)
    finish_times = [finish_times_flatten[i:i+n] for i in range(0, len(finish_times_flatten), n)]

    txs_per_block = int(filename_regex_groups['b'])
    txs_per_minute = txs_per_block / block_creation_avg_time
    # print(f'{filepath} : block_creation_avg={block_creation_avg_time / 60.}h, block_creation_std={block_creation_std_hours}h, block_creation_median_time={block_creation_median_time / 60.}, transactions_per_block={txs_per_block}, txs_per_minute={txs_per_minute}, iterations={len(times)}, {min(min(finish_times)) / 60.}h, {max(max(finish_times)) / 60.}h')

    print(LATEX_TABLE_HEADER)
    print_latex_row(os.path.basename(filepath), finish_times_flatten)
    print_latex_row(os.path.basename(filepath), times)

    return block_creation_avg_time, txs_per_minute, finish_times, times


if __name__ == '__main__':
    filepath = sys.argv[1]
    with open(filepath + '.avg_times', 'w') as f:
        f.write(' | '.join(map(str, get_information_from_logs(filepath))))
