from collections import defaultdict
import sys
import re
import numpy as np

filename_with_k_regex = r'[a-z0-9]+_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_k(?P<k>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[' \
                        r'0-9]+)_seed(?P<seed>[0-9]+).log'
filename_regex = r'[a-z0-9]+_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[0-9]+)_seed(?P<seed>[0-9]+).log'


def get_information_from_logs(filename):
    with open(filename, 'r') as f:
        # line = {LEVEL}:{FUNCTION}:{MESSAGE}
        lines = [line.split(':')[2] for line in f.readlines()]
        
        # filter only STAT
        lines = list(filter(lambda m: m.startswith('<STATS>'), lines))

    stats_regex = r'<STATS> max ABA round = ([0-9]+) ; average block creation = ([0-9]+.[0-9]+)'

    if filename.startswith('dumbo1'):
        filename_regex_groups = re.search(filename_with_k_regex, filename).groupdict()
    else:
        filename_regex_groups = re.search(filename_regex, filename).groupdict()

    n = int(filename_regex_groups['n'])
    
    block_creation_times_per_max_aba_round = defaultdict(lambda: [])

    all_block_creation_times = []
    for line in lines:
        s = re.search(stats_regex, line)
        max_round, block_creation_time = int(s.group(1)), float(s.group(2))
        block_creation_times_per_max_aba_round[max_round].append(block_creation_time)
        all_block_creation_times.append(block_creation_time)

    print(f'MEAN = {np.average(all_block_creation_times)}, STD = {np.std(all_block_creation_times)}')
    print(' {:10} | {:10} | {:10} | {:10} '.format('max_round', 'mean', 'std', 'count'))
    for max_round, times in sorted(block_creation_times_per_max_aba_round.items()):
        print(' {:10} | {:10f} | {:10f} | {:10} '.format(max_round, np.average(times), np.std(times), len(times)))


if __name__ == '__main__':
    filename = sys.argv[1]
    get_information_from_logs(filename)
