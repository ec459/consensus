from consensus.beat_crypto.threshenc.tdh2 import dealer, serialize, group, TDHPrivateKey, TDHPublicKey
import argparse
import pickle


def deserialize(pickled_enc_setup):
    l, k, vk, vks, sks = pickle.loads(pickled_enc_setup)
    vk = deserialize(vk)
    vks = [deserialize(vkp) for vkp in vks]
    sks = [(i, deserialize(sk)) for i, sk in sks]
    PK = TDHPublicKey(l, k, vk, vks)
    SKs = [TDHPrivateKey(l, k, vk, vks, sks[i], i) for i in range(len(sks))]
    return PK, SKs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('players', help='The number of players')
    parser.add_argument('k', help='k')
    args = parser.parse_args()
    players = int(args.players)
    if args.k:
        k = int(args.k)
    else:
        k = players / 2  # N - 2 * t
    PK, SKs = dealer(players=players, k=k)
    content = (PK.l, PK.k, serialize(PK.VK), [serialize(VKp) for VKp in PK.VKs],
               [(SK.i, serialize(SK.SK)) for SK in SKs])
    print(pickle.dumps(content))


if __name__ == '__main__':
    main()
