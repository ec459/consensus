from consensus.beat_crypto.commoncoin.thresprf import dealer, serialize, serialize1, group, deserialize, \
    TPRFPrivateKeyWithGG, TPRFPublicKeyWithGG
import argparse
import pickle


def deserialize_with_gg(pickled_sig_setup):
    l, k, vk, vks, sks, gg = pickle.loads(pickled_sig_setup)
    vk = deserialize(vk)
    vks = [deserialize(vkp) for vkp in vks]
    sks = [(i, deserialize(sk)) for i, sk in sks]
    gg = deserialize(gg)
    PK = TPRFPublicKeyWithGG(l, k, vk, vks, gg)
    SKs = [TPRFPrivateKeyWithGG(l, k, vk, vks, sks[i], i, gg) for i in range(len(sks))]
    return PK, SKs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('players', help='The number of players')
    parser.add_argument('k', help='k')
    args = parser.parse_args()
    players = int(args.players)
    if args.k:
        k = int(args.k)
    else:
        k = players / 2  # N - 2 * t
    PK, SKs, gg = dealer(players=players, k=k)
    content = (PK.l, PK.k, serialize1(PK.VK), [serialize1(VKp) for VKp in PK.VKs],
               [(SK.i, serialize1(SK.SK)) for SK in SKs], serialize1(gg))
    print(pickle.dumps(content))


if __name__ == '__main__':
    main()
