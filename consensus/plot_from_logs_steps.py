import os
import re
import matplotlib.pyplot as plt
import sys
from scipy.optimize import curve_fit
import numpy as np

filename_with_k_regex = r'(?P<c>[a-z0-9]+)_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_k(?P<k>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[' \
                        r'0-9a-z_A-Z-]+)_seed(?P<seed>[0-9]+).log'
filename_regex = r'(?P<c>[a-z0-9]+)_n(?P<n>[0-9]+)_f(?P<f>[0-9]+)_b(?P<b>[0-9]+)_nw(?P<nw>[0-9a-zA-Z_-]+)_seed(?P<seed>[0-9]+).log'

USE_HBBFT = False
CONSENSUS = (['hbbft'] if USE_HBBFT else []) + ['beat0', 'beat1', 'dumbo1']



def plot_avg_sim_time(datas_index_to_plot):
    labels = list(range(4, 31, 2))

    def get_stats(algo, n):
        algo_and_n_datas = list(filter(lambda v: v.split(',')[0] == algo and int(v.split(',')[1]) == n, datas))
        flatten_times = [v for l in algo_and_n_datas for v in datas[l][datas_index_to_plot]]
        print(algo, n, np.average(flatten_times))
        return np.average(flatten_times), np.std(flatten_times), np.max(flatten_times), np.min(flatten_times)

    means_per_algo, err_per_algo, max_per_algo, min_per_algo = {}, {}, {}, {}
    for algo in CONSENSUS:
        stats = [get_stats(algo, n) for n in labels]
        means_per_algo[algo] = [v[0] for v in stats]
        err_per_algo[algo] = [v[1] for v in stats]
        max_per_algo[algo] = [v[2] for v in stats]
        min_per_algo[algo] = [v[3] for v in stats]

    xs = np.arange(len(labels))
    width = 0.2

    fig, ax = plt.subplots()
    if USE_HBBFT:
        xs_and_algo = [(xs - 2 * width + width / 2, 'hbbft', 'C0'), (xs - width + width / 2, 'beat0', 'C1'), (xs + width - width / 2, 'beat1', 'C2'), (xs + 2 * width - width / 2, 'dumbo1', 'C3')]
        for (x, algo, color) in xs_and_algo:
            ax.scatter(x, max_per_algo[algo], linewidths=0.5, marker='.', c=color)
            ax.bar(x, means_per_algo[algo], width, yerr=err_per_algo[algo], label=algo, zorder=0, color=color)
            ax.scatter(x, min_per_algo[algo], linewidths=0.5, marker='.', c='black')
    else:
        xs_and_algo = [(xs - width, 'beat0', 'C1'), (xs, 'beat1', 'C2'), (xs + width, 'dumbo1', 'C3')]
        for (x, algo, color) in xs_and_algo:
            ax.scatter(x, max_per_algo[algo], linewidths=0.5, marker='.', c=color)
            ax.bar(x, means_per_algo[algo], width, yerr=err_per_algo[algo], label=algo, zorder=0, color=color)
            ax.scatter(x, min_per_algo[algo], linewidths=0.5, marker='.', c='black')
            #(a, b), _ = curve_fit(lambda t,a,b: a*np.exp(b*(t*2+4)), x, means_per_algo[algo])
            #exp_xs = np.linspace(0, 16, 100)
            #plt.plot(exp_xs, a * np.exp(b * (exp_xs*2+4)))

    if datas_index_to_plot == 3:
        ax.set_ylabel('Duración [segundos]')
        ax.set_title(f'Duración promedio de cada instancia de simulación')
    elif datas_index_to_plot == 2:
        ax.set_ylabel('Duración [horas]')
        ax.set_title(f'Duración promedio de la creación del bloque')

    ax.set_xlabel('Nodos')
    ax.set_xticks(xs)
    ax.set_xticklabels(labels)
    ax.legend()

    fig.tight_layout()
    plt.show()


def plot_latency(algo):
    labels = list(range(4, 31, 2))

    def get_mean_and_avg_of_sim(algo, n):
        sim_times = [np.average(v) for v in datas[f'{algo},{n}'][2]]
        print(algo, n, b, nw, sim_times, np.average(sim_times), np.std(sim_times))
        return np.average(sim_times), np.std(sim_times)

    for n in labels:
            avg, err = get_mean_and_avg_of_sim(algo, n)
            networks_means[nw].append(avg)
            networks_err[nw].append(err)

    fig, ax = plt.subplots()
    for i in range(len(networks_means)):
        ax.errorbar(range(len(labels)), networks_means[i], yerr=networks_err[i], linestyle='dotted', label=line_label[i])

    ax.set_ylabel('Minutos')
    ax.set_xlabel('Transacciones por bloque')
    ax.set_title(f'Latencia: {algo} con {n} nodos')
    ax.set_xticks(range(len(labels)))
    ax.set_xticklabels(labels)
    ax.legend()
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    log_files = [filename for filename in os.listdir(".") if filename.endswith('.log.avg_times')]
    datas = {}

    for filename in log_files:
        with open(filename, 'r') as f:
            line = f.read().strip().split(' | ')
            if line == [] or line == None or line == ['']:
                print(f'{filename} is empty.')
                continue
            latency, throughput, block_times, times = line
            latency, throughput = float(latency), float(throughput)
            times, block_times = eval(times), eval(block_times)

            if filename.startswith('dumbo1'):
                filename_regex_groups = re.search(filename_with_k_regex, filename).groupdict()
            else:
                filename_regex_groups = re.search(filename_regex, filename).groupdict()
            c, n = filename_regex_groups['c'], int(filename_regex_groups['n'])

            datas['{},{}'.format(c, n)] = (latency, throughput, block_times, times)

    plot_avg_sim_time(2)
    plot_avg_sim_time(3)

