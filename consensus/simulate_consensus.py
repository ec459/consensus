import argparse
import logging
import random
import string
import time
import sys
import json
import os

import numpy as np
import simpy
from simpy.resources.store import Store

from consensus.beat_crypto.commoncoin.thresprf import dealerWithGG as thresprf_dealer, TprfWithGgJsonEncoder, \
    TprfWithGgJsonDecoder
from consensus.beat_crypto.threshenc import tdh2
from consensus.core.beat import Beat
from consensus.core.dumbo1 import Dumbo1
from consensus.core.honeybadger import HoneyBadgerBFT
import consensus.core.vars
from consensus.crypto.threshenc import tpke
from consensus.crypto.threshsig.boldyreva import dealer as boldyreva_dealer, TblsJsonDecoder, TblsJsonEncoder
from consensus.simulation.consts import TRANSACTION_SIZE, CONSENSUS, ITERATION
from consensus.simulation.network_delay import SimpleDelayWithoutThroughput, SimpleDelayWithConstantThroughput
from consensus.simulation import network_contacts

logger = logging.getLogger(__name__)

SAMPLE_NETWORKS = [
    SimpleDelayWithoutThroughput,
    lambda n, f, seed: SimpleDelayWithConstantThroughput(n, f, seed, 256000),  # 256kbps
    lambda n, f, seed: SimpleDelayWithConstantThroughput(n, f, seed, 32000),  # 32kbps
]


def simple_router(n, env, network_delay):
    """Builds a set of connected channels, with delays depending on
    the network passed as argument.

    :param N: total number of nodes
    :param env: Simpy environment
    :param NetworkDelay network_delay: Simulated delay time between nodes

    :return: (receives, sends)
    """

    queues = [Store(env) for _ in range(n)]
    msgs = set()
    def make_send(i):
        """Returns a function that sends a message within the SimPy environment."""

        def _send(j, o):
            if i != j:
                delay = network_delay.get_delay(i, j, env.now, o)
                if delay < 0:
                    # logger.info(f'i={i} -> j={j}: t0={env.now} CAN_NOT_SEND')
                    return
                # logger.info(f'i={i} -> j={j}: t0={env.now} t1={env.now + delay} o={o}')
                consensus.core.vars.msg_sent += 1
                yield env.timeout(delay)
            queues[j].put((i, o))

        return lambda j, o: env.process(_send(j, o))

    def make_recv(j):
        def _recv():
            i, o = yield queues[j].get()
            return i, o

        def _recv2():
            return env.process(_recv())

        return _recv2

    return [make_send(i) for i in range(n)], [make_recv(j) for j in range(n)]


def test_consensus(n, f, b, env, consensus_name, network_delay, sig_setup, enc_setup, threads, k=None):
    txs_per_nodes = (b + n - 1) // n
    sid = 'sidA'

    sig_pk, sig_sks = sig_setup
    enc_pk, enc_sks = enc_setup

    sends, recvs = simple_router(n, env, network_delay)

    nodes = [None] * n

    for i in range(n):
        if consensus_name == 'hbbft':
            nodes[i] = HoneyBadgerBFT(sid, i, txs_per_nodes, n, f, sig_pk,
                                      sig_sks[i], enc_pk, enc_sks[i], sends[i],
                                      recvs[i], env)
        elif consensus_name == 'dumbo1':
            nodes[i] = Dumbo1(sid, i, txs_per_nodes, n, k, f, sig_pk,
                              sig_sks[i], enc_pk, enc_sks[i], sends[i],
                              recvs[i], env)
        elif consensus_name.startswith('beat'):
            beat_version = int(consensus_name[4:])
            nodes[i] = Beat(beat_version, sid, i, txs_per_nodes, n,
                            f, sig_pk, sig_sks[i], enc_pk, enc_sks[i],
                            sends[i], recvs[i], env)

        # Generate random transactions to propose for the new block
        for d in range(txs_per_nodes):
            random_prefix = ''.join(random.choice(string.ascii_letters)
                                    for _ in range(TRANSACTION_SIZE - 8))
            nodes[i].submit_tx('{}{:08d}'.format(random_prefix, i * txs_per_nodes + d))

        threads[i] = env.process(nodes[i].run())

    yield env.all_of(threads)
    outs = [threads[i].value for i in range(n) if threads[i].value is not None]
    # print(outs)

    logger.info(f'<REPORT> Done. {len(outs)} out of {n} nodes outputted a block.')
    logger.info(f'<REPORT> Total number of unique blocks {len(set(outs))}.')

    mx_aba_round = max([max(list(v.values())) for k, v in consensus.core.vars.aba_rounds.items()])
    block_creation_mean = np.average(list(consensus.core.vars.block_creations.values()))
    logger.info(f'<STATS> max ABA round = {mx_aba_round} ; average block creation = {block_creation_mean}')

    # Consistency check
    assert len(set(outs)) == 1


def build_filename(args):
    if args.network is not None:
        network = args.network
    else:
        network = os.path.splitext(os.path.basename(args.contacts_file))[0]

    if args.consensus == 'dumbo1':
        filename = f'dumbo1_n{args.n}_f{args.f}_k{args.k}_b{args.b}_nw' \
                   f'{network}_seed{args.seed}'
    elif args.consensus == 'hbbft':
        filename = f'hbbft_n{args.n}_f{args.f}_b{args.b}_nw' \
                   f'{network}_seed{args.seed}'
    elif args.consensus == 'beat0' or args.consensus == 'beat1':
        filename = f'{args.consensus}_n{args.n}_f{args.f}_b{args.b}_nw' \
                   f'{network}_seed{args.seed}'
    return filename


def loads_setup_with_custom_decoder(setups, decoder_cls):
    # print(type(list(map(json.loads, setups))[0][0]))
    return [
        (json.loads(json.dumps(setup[0]), cls=decoder_cls),
         [json.loads(json.dumps(sk), cls=decoder_cls) for sk in setup[1]])
        for setup in list(map(json.loads, setups))
    ]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', help='Total number of nodes', default=None, type=int)
    parser.add_argument('-f', help='Number of byzantine nodes', default=None, type=int, required=True)
    parser.add_argument('-k', help='Number of leader nodes for dumbo1', default=None, type=int)
    parser.add_argument('-b', default=4, type=int,
                        help='Transaction per block. Each node will propose '
                             'ceil(b / n) transactions')
    parser.add_argument('--contacts_file', help='CSV file with contacts', type=str)
    parser.add_argument('--iteration', help='Runs to simulate over network', default=ITERATION, type=int)
    parser.add_argument('--network', help='Sample network to run consensus on', type=int)
    parser.add_argument('--seed', help='Seed of random delay for random networks', default=None, type=int)
    parser.add_argument('--consensus', help='Consensus algorithm', choices=CONSENSUS, default='hbbft')
    parser.add_argument('--scale', help='Scale latency for contact networks', default=1, type=float)
    parser.add_argument('--setup', help='Use setup file containing encoding and signature.', default=None, type=str)

    args = parser.parse_args()

    # Additional checks on arguments

    # Check if only one network setup if assigned (random or from csv)
    assert (not (args.contacts_file and args.network))
    # If consensus is dumbo then k should be set
    assert (not args.consensus.startswith('dumbo') or args.k)

    if args.seed is None:
        args.seed = random.Random(None).getrandbits(31)

    if args.contacts_file is not None:
        network_delay = network_contacts.parse_csv_to_network_delay(args.contacts_file)
        # Override n with nodes from contacts file.
        args.n = network_delay.n
    elif args.network is not None:
        assert (args.n and args.n >= 3 * args.f + 1)
        assert (0 <= args.network < len(SAMPLE_NETWORKS))
        network_delay = SAMPLE_NETWORKS[args.network](args.n, args.f, args.seed)
    else:
        logger.error(f'Aborting since no CSV file or random network was setup up (--contacts_file or --network).')
        sys.exit(-1)

    # Setup logging
    filename = build_filename(args)
    log_output = open(f'{filename}.log', 'w')
    logging.basicConfig(stream=log_output, level=logging.INFO)

    # Additional network checks
    assert (not args.consensus.startswith('dumbo') or args.n >= args.k)

    # Setup signature and encoding seeds
    # Select threshold encoding and signature/coin tossing methods depending on consensus algorithm
    # pk = public key, sks = list of secret keys
    if args.setup:
        logger.info(f'<SETUP> Reading setup from {args.setup}.setup')
        with open(args.setup, 'r') as f:
            serialized_sig_setups, serialized_enc_setups = json.loads(f.read())

        if args.consensus.startswith('beat'):
            sig_setups = loads_setup_with_custom_decoder(serialized_sig_setups, TprfWithGgJsonDecoder)
            enc_setups = loads_setup_with_custom_decoder(serialized_enc_setups, tdh2.TdhJsonDecoder)
        else:
            sig_setups = loads_setup_with_custom_decoder(serialized_sig_setups, TblsJsonDecoder)
            enc_setups = loads_setup_with_custom_decoder(serialized_enc_setups, tpke.TpkeJsonDecoder)
    else:
        sig_setups, enc_setups = [], []

        if args.consensus.startswith('beat'):
            for i in range(args.iteration):
                sig_setups.append(thresprf_dealer(args.n, args.f + 1))
                enc_setups.append(tdh2.dealer(args.n, args.f + 1))
            serialized_sig_setups = [json.dumps(v, cls=TprfWithGgJsonEncoder) for v in sig_setups]
            serialized_enc_setups = [json.dumps(v, cls=tdh2.TdhJsonEncoder) for v in enc_setups]
        else:
            for i in range(args.iteration):
                sig_setups.append(boldyreva_dealer(args.n, args.f + 1, args.seed + i))
                enc_setups.append(tpke.dealer(args.n, args.f + 1))
            serialized_sig_setups = [json.dumps(v, cls=TblsJsonEncoder) for v in sig_setups]
            serialized_enc_setups = [json.dumps(v, cls=tpke.TpkeJsonEncoder) for v in enc_setups]

        with open(f'{filename}.setup', 'w') as f:
            f.write(json.dumps([serialized_sig_setups, serialized_enc_setups]))

    for it in range(args.iteration):
        env = simpy.Environment()
        network_delay.reset()
        threads = [None for i in range(args.n)]
        env.process(test_consensus(n=args.n, f=args.f, k=args.k, b=args.b, network_delay=network_delay, env=env,
                                   consensus_name=args.consensus, sig_setup=sig_setups[it], enc_setup=enc_setups[it],
                                   threads=threads))
        t0 = time.perf_counter()
        env.run()
        if any([not thread.processed for thread in threads]):
            finished = [thread.value for thread in threads if thread.processed]
            logger.info(f'<REPORT> Only {len(finished)} of {args.n} finished building the block')
            assert (not finished or len(set(finished)) == 1)

        logger.info(f'<TIME> Simulation duration = {time.perf_counter() - t0}')

    log_output.close()
