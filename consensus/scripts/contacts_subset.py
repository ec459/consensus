import csv
import sys


# Example: python consensus/scripts/contacts_subset.py contacts/interplanetary-access.csv
# contacts/earth-moon-mars.csv "DSN_Goldstone,DSN_Madrid,DSN_Canberra,Fac_Moon,Sat_Moon,Fac_Mars,Sat_Mars,Fac_Deimos,
# Sat_Deimos,Fac_Phobos,Sat_Phobos"

def nodes_in_subset(csv_line, subset):
    return csv_line[0] in subset and csv_line[1] in subset


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('python contacts_subset.py $input_file $output_file $nodes_subset_sep_by_comma')
        sys.exit(-1)

    subset = sys.argv[3].replace('"','').split(',')
    print(subset)

    with open(sys.argv[1], 'r') as csv_file:
        lines = list(csv.reader(csv_file))
        header = lines[0]
        lines = list(filter(lambda v: nodes_in_subset(v, subset), lines[1:]))

    with open(sys.argv[2], 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(header)
        writer.writerows(lines)
