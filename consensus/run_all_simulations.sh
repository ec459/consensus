#!/usr/bin/bash
# Warning: All previous simulation information will be deleted with `make clean`

export PYTHONPATH='.'

# TODO: Increase for final results
ITERATION=30

# Using N as 4*F
F="1 2 3 4"
B="100 500 1000 2500 5000 7500 10000"
# Networks with throughput
#NW = "1 2"
# Network 0 (no throughput)
# Network 1 (256kbps all connections)
# Network 2 (32kbps all connections)

# Clean up all previous simulation data
#make clean


########################
## BEAT0-1 simulations
########################
BEAT_VERSIONS="0 1"

# Network: CONTACTS_FILE0
for v in $BEAT_VERSIONS; do
for f in $F; do
	python consensus/simulate_consensus.py --consensus beat$v -f $f -b $((f * 4)) --contacts_file=$CONTACTS_FILE0 --seed 24
	echo "DONE beat$v n=$((f * 4)) f=$f b=100 network=$CONTACTS_FILE0 seed=24"
done
done

# Network 0: no throughput
for v in $BEAT_VERSIONS; do
for f in $F; do
	python consensus/simulate_consensus.py --consensus beat$v -n $((f * 4)) -f $f -b $((f * 4)) --network 0 --seed 24
	echo "DONE beat$v n=$((f * 4)) f=$f b=100 network=0 seed=24"
done
done

# Rest of the networks
for v in $BEAT_VERSIONS; do
for nw in $NW; do
for f in $F; do
for b in $B; do
	python consensus/simulate_consensus.py --consensus beat$v -n $((f * 4)) -f $f -b $b --network $nw --seed 24
	echo "DONE beat$v n=$((f * 4)) f=$f b=$b network=$nw seed=24"
done
done
done
done

########################
## HBBFT simulations
########################

# Network: CONTACTS_FILE0
for f in $F; do
	python consensus/simulate_consensus.py --consensus hbbft -n $((f * 4)) -f $f -b 100 --contacts_file=$CONTACTS_FILE0 --seed 24
	echo "DONE hbbft n=$((f * 4)) f=$f b=100 network=$CONTACTS_FILE0 seed=24"
done

# Network 0: no throughput
for f in $F; do
	python consensus/simulate_consensus.py --consensus hbbft -n $((f * 4)) -f $f -b 100 --network 0 --seed 24
	echo "DONE hbbft n=$((f * 4)) f=$f b=100 network=0 seed=24"
done

# Rest of the networks
for nw in {1,2}; do
for f in $F; do
for b in $B; do
	python consensus/simulate_consensus.py --consensus hbbft -n $((f * 4)) -f $f -b $b --network $nw --seed 24
	echo "DONE beat$v n=$((f * 4)) f=$f b=$b network=$nw seed=24"
done
done
done


######################
# Dumbo1 simulations
######################

# Network: CONTACTS_FILE0
for f in $F; do
	python consensus/simulate_consensus.py --consensus dumbo1 -f $f -k $((f+1)) -b 100 --contacts_file=$CONTACTS_FILE0 --seed 24
	echo "DONE dumbo1 n=$((f * 4)) f=$f k=$((f+1)) b=100 network=$CONTACTS_FILE0 seed=24"
done

# Network 0
for f in $F; do
	python consensus/simulate_consensus.py --consensus dumbo1 -n $((f * 4)) -f $f -k $((f+1)) -b 100 --network 0 --seed 24
	echo "DONE dumbo1 n=$((f * 4)) f=$f b=100 network=0 seed=24"
done

# Rest of the networks
for nw in $NW; do
for f in $F; do
for b in $B; do
	python consensus/simulate_consensus.py --consensus dumbo1 -n $((f * 4)) -f $f -k $((f+1)) -b $b --network $nw --seed 24
	echo "DONE dumbo1 n=$((f * 4)) f=$f k=$((f+1)) b=$b network=$nw seed=24"
done
done
done

##########################
## Analyze logs for plots
##########################
#
#for file in $(ls | grep ".log$"); do
#  python avg_finish_time.py $file
#done
